#!/bin/bash

grep 'image:' docker-compose.yml | awk '{ print $2 }' | while read -r image; do
    echo "image: $image"
    docker pull "$image"
done
