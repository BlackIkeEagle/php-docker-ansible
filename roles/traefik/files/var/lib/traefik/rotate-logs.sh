#!/usr/bin/env bash

# move yesterday's logs in its own file
grep "$(date '+%d/%b/%Y' -d yesterday)" /var/lib/traefik/data/logs/access.log \
    | sponge /var/lib/traefik/data/logs/access-"$(date '+%Y-%m-%d' -d yesterday)".log

# truncate access log to todays logs
grep "$(date '+%d/%b/%Y')" /var/lib/traefik/data/logs/access.log \
    | sponge /var/lib/traefik/data/logs/access.log

# delete split logs older than 40 days
find /var/lib/traefik/data/logs/ -iname "access-*.log" -mtime +40 -delete
