#!/usr/bin/env bash

# today
/usr/bin/grep "$(date '+%d/%b/%Y')" /var/lib/traefik/data/logs/access.log \
    | sponge \
    | /usr/local/bin/goaccess-traefik - -a -o /var/lib/goaccess/data/html/today.html

# all-time
/usr/bin/mkdir -p /var/lib/goaccess/data/db/all-time.db
/usr/local/bin/goaccess-traefik \
    --db-path=/var/lib/goaccess/data/db/all-time.db \
    --restore  \
    --persist \
    -f /var/lib/traefik/data/logs/access.log \
    -a -o /var/lib/goaccess/data/html/all-time.html
