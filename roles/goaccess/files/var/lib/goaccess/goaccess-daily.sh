#!/usr/bin/env bash

# yesterday
yesterday="$(date '+%Y-%m-%d' -d yesterday)"
yesterday_folder="$(date '+%Y/%m/%d' -d yesterday)"
month_folder="$(date '+%Y/%m' -d yesterday)"
year_folder="$(date '+%Y' -d yesterday)"

logfolder="/var/lib/traefik/data/logs"
dbfolder="/var/lib/goaccess/data/db"
htmlfolder="/var/lib/goaccess/data/html"

# create folders
/usr/bin/mkdir -p "$dbfolder/$month_folder"
/usr/bin/mkdir -p "$htmlfolder/$month_folder"

if [[ ! -e "$logfolder/access-$yesterday.log" ]]; then
    exit 1;
fi

# generate log of day
/usr/local/bin/goaccess-traefik -a \
    -f "$logfolder/access-$yesterday.log" \
    -o "$htmlfolder/$yesterday_folder.html"

# generate log of month
/usr/local/bin/goaccess-traefik --db-path="$dbfolder/$month_folder" \
    --restore  \
    --persist \
    -a \
    -f "$logfolder/access-$yesterday.log" \
    -o "$htmlfolder/$month_folder/month.html"

# generate log of year
/usr/local/bin/goaccess-traefik --db-path="$dbfolder/$year_folder" \
    --restore  \
    --persist \
    -a \
    -f "$logfolder/access-$yesterday.log" \
    -o "$htmlfolder/$year_folder/year.html"
